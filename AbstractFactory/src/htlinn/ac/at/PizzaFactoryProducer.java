package htlinn.ac.at;

public class PizzaFactoryProducer {
    public static PizzaFabrik getFabrik(boolean isReal)
    {
        if(isReal)
        {
            return new RealPizzaFabrik();
        }else
        {
            return new UnrealPizzaFabrik();
        }
    }
}
