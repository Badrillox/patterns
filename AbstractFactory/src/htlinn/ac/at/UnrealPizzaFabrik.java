package htlinn.ac.at;

public class UnrealPizzaFabrik extends PizzaFabrik{
    @Override
    public Pizza getPizza(PizzaTyp typ) {
        if (typ == PizzaTyp.Diavolo)
        {
            return new Diavolo();
        }
        if(typ == PizzaTyp.Salami)
        {
            return new Salami();
        }
        return null;
    }
}
