package htlinn.ac.at;

public abstract class PizzaFabrik {
    public abstract Pizza getPizza(PizzaTyp pizzaType);
}
