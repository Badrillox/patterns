package htlinn.ac.at;

public class RealPizzaFabrik extends PizzaFabrik{

    @Override
    public Pizza getPizza(PizzaTyp typ) {
        if (typ == PizzaTyp.Margarita)
        {
            return new Margarita();
        }
        if(typ == PizzaTyp.Quaddro)
        {
            return new Quaddro();
        }
        return null;
    }
}
