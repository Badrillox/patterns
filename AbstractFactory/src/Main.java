import htlinn.ac.at.*;
public class Main {
    public static void main(String[] args) {
        //get shape factory
        PizzaFabrik unrealFactory = PizzaFactoryProducer.getFabrik(false);
        //get an object of Shape Rectangle
        Pizza oidaIsDesHoas = unrealFactory.getPizza(PizzaTyp.Diavolo);
        //call draw method of Shape Rectangle
        oidaIsDesHoas.backen();

        Pizza extraSalami = unrealFactory.getPizza(PizzaTyp.Salami);
        extraSalami.backen();

        //Same with other factory typ
        PizzaFabrik realFactory = PizzaFactoryProducer.getFabrik(true);
        Pizza oidaIglabDaIsnixdrauf = realFactory.getPizza(PizzaTyp.Margarita);
        oidaIglabDaIsnixdrauf.backen();

        Pizza kaWasQuaddro = realFactory.getPizza(PizzaTyp.Quaddro);
        kaWasQuaddro.backen();
    }
}