package htlinn.ac.at;

public interface IPizza {
    String getBeschreibung();
    double getPreis();
}
