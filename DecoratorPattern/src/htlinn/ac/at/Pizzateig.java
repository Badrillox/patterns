package htlinn.ac.at;

public class Pizzateig implements IPizza {

    @Override
    public String getBeschreibung() {
        return "StdTeig";
    }

    @Override
    public double getPreis() {
        return 2.0;
    }
}
