package htlinn.ac.at;

public class Cheesy implements IPizza {

    IPizza base;

    public Cheesy(IPizza c){
        base = c;
    }
    @Override
    public String getBeschreibung() {
        return base.getBeschreibung() + " mit Käse";
    }

    @Override
    public double getPreis() {
        return base.getPreis() + 3.0;
    }
}
