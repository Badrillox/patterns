package htlinn.ac.at;

public class Tomaten implements IPizza {

    IPizza base;
    public Tomaten(IPizza b) {
        base = b;
    }
    @Override
    public String getBeschreibung() {
        return base.getBeschreibung() + " mit Tomaten";
    }

    @Override
    public double getPreis() {
        return base.getPreis() + 1.0; // Basispreis + Veränderung --- sind mir eig in da 2. Klasse?
    }
}
