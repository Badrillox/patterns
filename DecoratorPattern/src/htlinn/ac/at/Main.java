package htlinn.ac.at;

public class Main {
    public static void main(String[] args) {
        IPizza p = new Pizzateig();
        p = new Tomaten(p);
        p = new Cheesy(p);

        System.out.println(p.getBeschreibung());
        System.out.println(p.getPreis());
    }
}
