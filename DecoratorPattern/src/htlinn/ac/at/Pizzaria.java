package htlinn.ac.at;

public class Pizzaria {
    // Factory Method
    // static Functions haben kein dynamic Binding ...
    // ... kann auch nicht vererbt überschrieben werden
    // gehört der Klasse an, nicht als Objekt aufzurufen
    static IPizza getMargherita(){
        IPizza p = new Pizzateig();
        p = new Tomaten(p);
        p = new Cheesy(p);

        return p;
    }
}
