import htlinn.ac.at.*;

public class Main {
    public static void main(String[] args) {
        System.out.println("Was kann schon schief gehen?");
        Bohrer b = new Bohrer();
        CommandRecorder cmdRec = new CommandRecorder();

        cmdRec.execute(new VorCMD(b, 10));
        cmdRec.execute(new SenkenCMD(b));
        cmdRec.execute(new DrehenCMD(b, 45));
        cmdRec.execute(new VorCMD(b, 30));
        cmdRec.execute(new HebenCMD(b));
        cmdRec.execute(new VorCMD(b, 30));
        cmdRec.undo();

    }
}
