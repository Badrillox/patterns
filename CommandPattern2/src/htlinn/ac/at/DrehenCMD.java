package htlinn.ac.at;

public class DrehenCMD implements Command{
    Bohrer obj;
    int grad;

    public DrehenCMD(Bohrer obj, int grad) {
        this.obj = obj;
        this.grad = grad;
    }

    @Override
    public void execute() {
        obj.drehenL(this.grad);
    }

    @Override
    public void undo() {
        obj.drehenR(this.grad);
    }
}
