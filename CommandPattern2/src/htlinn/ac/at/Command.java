package htlinn.ac.at;

public interface Command {
    void execute();
    void undo();
}
