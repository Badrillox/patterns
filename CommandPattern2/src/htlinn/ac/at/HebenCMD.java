package htlinn.ac.at;

public class HebenCMD implements Command {

    Bohrer obj;

    public HebenCMD(Bohrer obj) {
        this.obj = obj;
    }

    @Override
    public void execute() {
        obj.heben();
    }

    @Override
    public void undo() {

    }
}
