package htlinn.ac.at;

public class Bohrer {

    public void vor(int mm){
        System.out.println("Bewegung nach vorne: " + mm + " mm");
    }

    public void drehenL(int grad){
        System.out.println("Drehen nach links: " + grad + " °");
    }

    public void drehenR(int grad){
        System.out.println("Drehen nach rechts " + grad + " °");
    }

    public void senken(){

    }

    public void heben(){

    }

}
