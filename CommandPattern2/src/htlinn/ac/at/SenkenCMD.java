package htlinn.ac.at;

public class SenkenCMD implements Command{
    Bohrer obj;

    public SenkenCMD(Bohrer obj) {
        this.obj = obj;
    }

    @Override
    public void execute() {
        obj.senken();
    }

    @Override
    public void undo() {
        obj.heben(); //werd scho passn
    }
}
