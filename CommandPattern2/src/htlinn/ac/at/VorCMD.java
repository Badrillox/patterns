package htlinn.ac.at;

public class VorCMD implements Command {

    Bohrer obj;
    int mm;

    public VorCMD(Bohrer obj, int mm) {
        this.obj = obj;
        this.mm = mm;
    }

    @Override
    public void execute() {
        obj.vor(mm);
    }

    @Override
    public void undo() {
        obj.drehenL(180);
        obj.vor(mm);
        obj.drehenL(180);
    }
}
