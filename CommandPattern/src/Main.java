import htlinn.ac.at.*;

public class Main {
    public static void main(String[] args) {
        WebRequest wr = new WebRequest();

        GetRequestCommand c1 = new GetRequestCommand(wr, "Seite 1");
        GetRequestCommand c2 = new GetRequestCommand(wr, "Seite 2");
        GetRequestCommand c4 = new GetRequestCommand(wr, "Seite 3");

        CommandRecorder cmdRec = new CommandRecorder();
        cmdRec.execute(c1);
        cmdRec.execute(c2);
        cmdRec.undo();
        cmdRec.redo();
    }
}
