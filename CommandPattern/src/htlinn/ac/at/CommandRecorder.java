package htlinn.ac.at;

import java.util.Stack;

public class CommandRecorder {
    Stack<Command> undoStack = new Stack<>();
    Stack<Command> redoStack = new Stack<>();

    public void execute(Command cmd){
        cmd.execute();
        undoStack.push(cmd);
        redoStack.clear(); // leert den ganzen Stack
    }
    public void undo(){
        Command cmd = undoStack.pop(); // pop nimmt es vom Stack runter
        redoStack.push(cmd);
        cmd = undoStack.peek(); // oberste Elemente und lässts am Stack
        cmd.execute();
    }
    public void redo(){
        Command cmd = redoStack.pop();
        cmd.execute();
        undoStack.push(cmd);
    }
    public boolean canUndo(){
        return undoStack.size() > 0; // wenn ein Element drauf ist, kann ich was zurückliefern
    }
    public boolean canRedo(){
        return redoStack.size() > 0;
    }
}
