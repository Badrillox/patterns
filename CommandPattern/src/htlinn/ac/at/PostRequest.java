package htlinn.ac.at;

public class PostRequest implements Command {

    WebRequest obj;
    String url;
    String params;

    public PostRequest(WebRequest obj, String url, String params) {
        this.obj = obj;
        this.url = url;
        this.params = params;
    }

    @Override
    public void execute() {
        obj.postRequest(url, params);
    }
}
