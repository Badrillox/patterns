package htlinnacat;

public class Enten {
    Lautverhalten v;

    protected Enten(Lautverhalten v){
        this.v = v;
    }
    void gibLaut(){
        // Delegate
        v.lautGeben();
    }
    public Lautverhalten getLautverhalten(){
        return v;
    }
    public void setLautverhalten(Lautverhalten v){
        this.v = v;
    }
}
