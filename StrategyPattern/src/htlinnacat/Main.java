package htlinnacat;

public class Main {
    public static void main(String[] args) {
        GummiEnte g = new GummiEnte();

        g.gibLaut();

        g.setLautverhalten(new Quaken());
        g.gibLaut();

        HolzEnte h = new HolzEnte();
        h.gibLaut();
    }
}
