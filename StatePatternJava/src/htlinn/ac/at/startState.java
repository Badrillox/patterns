package htlinn.ac.at;

public class startState implements State{

    @Override
    public void doAction(Context context) {
        System.out.println("start state is current state");
        context.setState(this);
    }

    @Override
    public String toString() {
        return "startState";
    }
}
