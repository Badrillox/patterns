package htlinn.ac.at;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
	// write your code here

        Context context = new Context();
        List<State> stateList = new ArrayList<>();

        startState startState = new startState();
        moneyReachedState moneyReachedState = new moneyReachedState();
        dropItemState dropItemState = new dropItemState();

        stateList.add(startState);
        stateList.add(moneyReachedState);
        stateList.add(dropItemState);

        stateList.forEach((State cstate) ->
                {
                    cstate.doAction(context);
                    System.out.println(context.getState().toString());
                }

        );

        System.out.println(context.getState().toString());

    }
}
