package htlinn.ac.at;

public class moneyReachedState implements State{

    @Override
    public void doAction(Context context) {
        System.out.println("moneyReached is current state");
        context.setState(this);
    }

    @Override
    public String toString() {
        return "moneyReachedState";
    }
}
