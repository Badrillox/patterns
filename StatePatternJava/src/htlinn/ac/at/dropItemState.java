package htlinn.ac.at;

public class dropItemState implements State {

    @Override
    public void doAction(Context context) {
        System.out.println("drop item is current state");
        context.setState(this);
    }

    @Override
    public String toString() {
        return "dropItemState";
    }
}
