package htlinn.ac.at;

public class Context {
    private State _state = null;

    public Context()
    {
        this._state = null;
    }

    public void setState(State input_state)
    {
        this._state = input_state;
    }

    public State getState()
    {
        return this._state;
    }
}
