package Decorator_Factory_Pattern;

public class R8_Spyder implements RennautoITF {
    RennautoITF rennautoITF;

    public R8_Spyder(RennautoITF rennautoITF) {
        this.rennautoITF = rennautoITF;
    }

    @Override
    public MotorTyp getMotorTyp() {
        return MotorTyp.Benzin;
    }

    @Override
    public String getGestellTyp() {
        return "gedämpftes Gestell";
    }

    @Override
    public String getHersteller() {
        return "Audi";
    }

    @Override
    public double getPreis() {
        return 2000000.00;
    }

    @Override
    public int getPS() {
        return 560;
    }

    @Override
    public String getBeschreibung() {
        return "Rennauto für reiche Menschen";
    }
}
