package Decorator_Factory_Pattern;

public class StdAuto implements AutoITF {
    @Override
    public MotorTyp getMotortyp() {
        return MotorTyp.Benzin;
    }

    @Override
    public int getSitze() {
        return 4;
    }

    @Override
    public int getTüren() {
        return 4;
    }

    @Override
    public String getFahrgestellnummer() {
        return "0000aWDAWd1";
    }

    @Override
    public double getPreis() {
        return 10000.00;
    }

    @Override
    public String getBeschreibung() {
        return "Std Auto";
    }

    @Override
    public int getPS() {
        return 60;
    }
}
