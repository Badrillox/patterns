package Decorator_Factory_Pattern;

public interface AutoITF {
    MotorTyp getMotortyp();
    int getSitze();
    int getTüren();
    String getFahrgestellnummer();
    double getPreis();
    String getBeschreibung();
    int getPS();
}
