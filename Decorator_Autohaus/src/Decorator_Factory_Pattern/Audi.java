package Decorator_Factory_Pattern;

public class Audi implements AutoITF {
    AutoITF autoITF;
    public Audi(AutoITF a)
    {
        autoITF = a;
    }

    @Override
    public MotorTyp getMotortyp() {
        return autoITF.getMotortyp();
    }

    @Override
    public int getSitze() {
        return autoITF.getSitze() + 1;
    }

    @Override
    public int getTüren() {
        return autoITF.getTüren();
    }

    @Override
    public String getFahrgestellnummer() {
        return autoITF.getFahrgestellnummer() + " ekjfkdjfjdfjdskjfkjdf454845";
    }


    @Override
    public double getPreis() {
        return autoITF.getPreis() + 25000.00;
    }

    @Override
    public String getBeschreibung() {
        return autoITF.getBeschreibung() + " Audi A3 8V 190PS";
    }

    @Override
    public int getPS() {
        return autoITF.getPS() + 130;
    }
}
