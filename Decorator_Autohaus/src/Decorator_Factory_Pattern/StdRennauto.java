package Decorator_Factory_Pattern;

public class StdRennauto implements RennautoITF{
    @Override
    public MotorTyp getMotorTyp() {
        return null;
    }

    @Override
    public String getGestellTyp() {
        return null;
    }

    @Override
    public String getHersteller() {
        return null;
    }

    @Override
    public double getPreis() {
        return 0;
    }

    @Override
    public int getPS() {
        return 0;
    }

    @Override
    public String getBeschreibung() {
        return null;
    }
}
