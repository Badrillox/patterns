package Decorator_Factory_Pattern;

public enum MotorTyp {
    Diesel, Benzin, Elekto, Wasserstoff, Hybrid, Gas, NotSpecified
}
