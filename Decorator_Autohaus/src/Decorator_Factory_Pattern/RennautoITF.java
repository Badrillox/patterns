package Decorator_Factory_Pattern;

public interface RennautoITF {
    MotorTyp getMotorTyp();
    String getGestellTyp();
    String getHersteller();
    double getPreis();
    int getPS();
    String getBeschreibung();

}
