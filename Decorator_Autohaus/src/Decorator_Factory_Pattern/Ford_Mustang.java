package Decorator_Factory_Pattern;

public class Ford_Mustang implements KlassikerITF{
    KlassikerITF klassikerITF;
    public Ford_Mustang(KlassikerITF kl){
        klassikerITF = kl;
    }
    @Override
    public MotorTyp getMotorTyp() {
        return MotorTyp.Diesel;
    }

    @Override
    public int getPS() {
        return 75;
    }

    @Override
    public String getFahrgestellnummer() {
        return "kmdfndmfdkAlteSchrottkiste1265454";
    }

    @Override
    public int getSitze() {
        return 2;
    }

    @Override
    public boolean getBenutzt() {
        return true;
    }

    @Override
    public double getPrice() {
        return 7500;
    }

    @Override
    public String getBeschreibung() {
        return "lahme Arschkiste hahihaiFischkind";
    }
}
