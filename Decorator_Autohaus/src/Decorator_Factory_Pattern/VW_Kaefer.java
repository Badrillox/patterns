package Decorator_Factory_Pattern;

public class VW_Kaefer implements KlassikerITF{
    @Override
    public MotorTyp getMotorTyp() {
        return MotorTyp.Diesel;
    }

    @Override
    public int getPS() {
        return 45;
    }

    @Override
    public String getFahrgestellnummer() {
        return "Käfer 890815awdawd";
    }

    @Override
    public int getSitze() {
        return 2;
    }

    @Override
    public boolean getBenutzt() {
        return true;
    }

    @Override
    public double getPrice() {
        return 8000.00;
    }

    @Override
    public String getBeschreibung() {
        return "Isch halt da Thomas als Auto";
    }
}
