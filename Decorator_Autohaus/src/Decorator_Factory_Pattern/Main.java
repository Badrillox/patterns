package Decorator_Factory_Pattern;

public class Main {
    public static void main(String[] args) {
        AutoITF autoITF = Autohaus.getAuto();
        System.out.println(autoITF.getMotortyp());
        System.out.println(autoITF.getSitze());
        System.out.println(autoITF.getPreis());
        System.out.println(autoITF.getFahrgestellnummer());
        System.out.println(autoITF.getTüren());
        System.out.println(autoITF.getBeschreibung());
        System.out.println(autoITF.getPS());
        System.out.println();

        RennautoITF rennautoITF = Autohaus.getRennauto();
        System.out.println(rennautoITF.getBeschreibung());
        System.out.println(rennautoITF.getPS());
        System.out.println(rennautoITF.getPreis());
        System.out.println(rennautoITF.getMotorTyp());
        System.out.println(rennautoITF.getGestellTyp());
        System.out.println(rennautoITF.getHersteller());
        System.out.println();

        KlassikerITF klassikerITF = Autohaus.getKlassiker();
        System.out.println(klassikerITF.getBenutzt());
        System.out.println(klassikerITF.getBeschreibung());
        System.out.println(klassikerITF.getFahrgestellnummer());
        System.out.println(klassikerITF.getPS());
        System.out.println(klassikerITF.getSitze());
        System.out.println(klassikerITF.getMotorTyp());
        System.out.println(klassikerITF.getPrice());
    }
}
