package Decorator_Factory_Pattern;

public interface KlassikerITF {
    MotorTyp getMotorTyp();
    int getPS();
    String getFahrgestellnummer();
    int getSitze();
    boolean getBenutzt();
    double getPrice();
    String getBeschreibung();
}
