package Decorator_Factory_Pattern;

public class StdKlassiker implements KlassikerITF{
    @Override
    public MotorTyp getMotorTyp() {
        return null;
    }

    @Override
    public int getPS() {
        return 0;
    }

    @Override
    public String getFahrgestellnummer() {
        return null;
    }

    @Override
    public int getSitze() {
        return 0;
    }

    @Override
    public boolean getBenutzt() {
        return false;
    }

    @Override
    public double getPrice() {
        return 0;
    }

    @Override
    public String getBeschreibung() {
        return null;
    }
}
