package Decorator_Factory_Pattern;

public class Autohaus {
    static AutoITF getAuto()
    {
        AutoITF a = new StdAuto();
        a = new Audi(a);
        return a;
    }
    static RennautoITF getRennauto()
    {
        RennautoITF r = new StdRennauto();
        r = new R8_Spyder(r);
        return r;
    }
    static KlassikerITF getKlassiker()
    {
        KlassikerITF k = new StdKlassiker();
        k = new Ford_Mustang(k);
        return k;
    }
}
