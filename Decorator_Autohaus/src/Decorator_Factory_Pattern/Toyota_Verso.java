package Decorator_Factory_Pattern;

public class Toyota_Verso implements AutoITF{
    AutoITF autoITF;
    public Toyota_Verso(AutoITF a)
    {
        autoITF = a;
    }

    @Override
    public MotorTyp getMotortyp() {
        return MotorTyp.Hybrid;
    }

    @Override
    public int getSitze() {
        return 6;
    }

    @Override
    public int getTüren() {
        return 4;
    }

    @Override
    public String getFahrgestellnummer() {
        return "Toyota 3678192AWhdAw";
    }

    @Override
    public double getPreis() {
        return 45000.00;
    }

    @Override
    public String getBeschreibung() {
        return "Isch halt Toyota";
    }

    @Override
    public int getPS() {
        return 80;
    }
}
